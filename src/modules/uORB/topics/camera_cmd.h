/*
 * camera.h
 *
 *  Created on: Dec 1, 2014
 *      Author: NUS_UAV
 */

#ifndef CAMERA_H_
#define CAMERA_H_
#include "../uORB.h"

/**
 * @addtogroup topics
 * @{
 */

struct camera_cmd_s {
	uint64_t t_timestamp;
	bool take_one_photo;
	bool continuous_take_photo;
	bool stop_take_photo;
	bool take_video;
	bool stop_video;
	int  pitch_control; //1 for up, 2 for down and 0 for nothing;
	int  yaw_control;
	float zoom;	//1 for zoom in, 2 for zoom out and 0 for nothing;
	float joystick_roll;
	float joystick_pitch;
	float joystick_yaw;
	float joystick_throttle;
	float acceleration;
}; /**< Velocity setpoint in NED frame */

/**
 * @}
 */

/* register this as object request broker structure */
ORB_DECLARE(camera_cmd);




#endif /* CAMERA_H_ */
