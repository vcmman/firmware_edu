/*
 * @file px4_simple_app.c
 * Minimal application example for PX4 autopilot.
 *
 *  Created on: Oct 13, 2014
 *      Author: Administrator
 */

#include <nuttx/config.h>
#include <stdio.h>
#include <unistd.h>
#include <poll.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <fcntl.h>
#include <nuttx/sched.h>
#include <arch/board/board.h>
#include <sys/prctl.h>
#include <termios.h>
#include <math.h>
#include <float.h>
#include <uORB/uORB.h>
#include <uORB/uORB.h>
#include <geo/geo.h>
#include <systemlib/systemlib.h>
#include <systemlib/err.h>
#include <drivers/drv_hrt.h>
#include <uORB/topics/sensor_combined.h>
#include <uORB/topics/vehicle_attitude.h>
#include <uORB/topics/vehicle_local_position.h>
#include <uORB/topics/vehicle_global_position.h>
#include <uORB/topics/vehicle_local_position_setpoint.h>
#include <uORB/topics/vehicle_global_velocity_setpoint.h>
#include <uORB/topics/vehicle_attitude_setpoint.h>
#include <uORB/topics/vehicle_attitude.h>
#include <uORB/topics/home_position.h>
#include <uORB/topics/actuator_controls.h>
#include <lib/mathlib/mathlib.h>

static bool thread_should_exit = false;		/**< daemon exit flag */
static bool thread_running = false;		/**< daemon status flag */
static int daemon_task;				/**< Handle of daemon task / thread */

 __EXPORT int px4_simulation_main(int argc, char *argv[]);


static void usage(const char *reason);
int px4_simulation_thread_main(int argc, char *argv[]);
static void
usage(const char *reason)
{
	if (reason)
		warnx("%s\n", reason);
	errx(1, "usage: daemon {start|stop|status} [-p <additional params>]\n\n");
}


int px4_simulation_thread_main(int argc, char *argv[])
{
	thread_running = true;
	int _local_pos_sp_sub = orb_subscribe(ORB_ID(vehicle_local_position_setpoint));
	int _global_vel_sp_sub = orb_subscribe(ORB_ID(vehicle_global_velocity_setpoint));
	int _att_sp_sub = orb_subscribe(ORB_ID(vehicle_attitude_setpoint));
	int _home_sub = orb_subscribe(ORB_ID(home_position));
	int _actuator_sub = orb_subscribe(ORB_ID(actuator_controls_1));

	struct vehicle_global_velocity_setpoint_s _global_vel_sp;
	struct vehicle_local_position_setpoint_s  _local_pos_sp;
	struct vehicle_local_position_s _local_pos;
	struct vehicle_global_position_s _global_pos;
	struct vehicle_attitude_setpoint_s _att_sp;
	struct vehicle_attitude_s _att;
	struct vehicle_attitude_s _att_temp;
	struct map_projection_reference_s ref;
	struct home_position_s _home;
	struct actuator_controls_s _act;


	memset(&_local_pos, 0, sizeof(_local_pos));
	memset(&_global_pos, 0, sizeof(_global_pos));
	memset(&_att,0,sizeof(_att));
	memset(&_global_vel_sp,0,sizeof(_global_vel_sp));
	memset(&_local_pos_sp,0,sizeof(_local_pos_sp));
	memset(&_att_sp,0,sizeof(_att_sp));
	memset(&_home,0,sizeof(_home));
	memset(&_act,0,sizeof(_act));


//	_home.lat = 1.40999399999999997;
//	_home.lon = 103.741804999999999;

	_home.lat = 1.329450;
	_home.lon = 103.787450;
	orb_advert_t _local_pos_pub = orb_advertise(ORB_ID(vehicle_local_position), &_local_pos);
	orb_advert_t _global_pos_pub = orb_advertise(ORB_ID(vehicle_global_position),&_global_pos);
	orb_advert_t _att_pub = orb_advertise(ORB_ID(vehicle_attitude),&_att);
	float thrust;
	float accx_body,accy_body;
	float accx_global, accy_global;
	bool updated;
	float dt;
	hrt_abstime t_prev;
	bool landed = true;
	float landed_time = 0.0f;
	float alt_avg = 0.0f;

//    float x_kalm_est,y_kalm_est,vx_kalm_est,vy_kalm_est;

	int loop_count = 0;
	orb_set_interval(_local_pos_sp_sub, 4);

//	struct pollfd fds[] = {
//			{ .fd = _local_pos_sp_sub, .events = POLLIN },
//
//	};


	while (!thread_should_exit) {

		    usleep(5000);

				orb_check(ORB_ID(vehicle_local_position_setpoint),&updated);
				if(updated)
					orb_copy(ORB_ID(vehicle_local_position_setpoint),_local_pos_sp_sub,&_local_pos_sp);

				orb_check(ORB_ID(vehicle_global_velocity_setpoint),&updated);
				if(updated)
					orb_copy(ORB_ID(vehicle_global_velocity_setpoint),_global_vel_sp_sub,&_global_vel_sp);

				orb_check(ORB_ID(home_position),&updated);
				if(updated)
					orb_copy(ORB_ID(home_position),_home_sub,&_home);

				orb_check(ORB_ID(vehicle_attitude_setpoint),&updated);
				if(updated)
				orb_copy(ORB_ID(vehicle_attitude_setpoint),_att_sp_sub,&_att_sp);

				map_projection_init(&ref,_home.lat, _home.lon);

				orb_check(_actuator_sub,&updated);
				if(updated){
					orb_copy(ORB_ID(actuator_controls_1),_actuator_sub,&_act);
				}

//				orb_copy(ORB_ID(other_uav_position),_other_uav_pos_sub,&_other_uav_pos);
//				orb_check(_other_uav_pos_sub,&_other_uav_pos_updated);
//				if(_other_uav_pos_updated)
//				{
////					printf("I am here 3\n");
////					usleep(5000);
//					orb_copy(ORB_ID(other_uav_position),_other_uav_pos_sub,&_other_uav_pos);
//
//					if(got_other_uav_pos == false){
//						map_projection_project(&ref,_other_uav_pos.lat,_other_uav_pos.lon,&x_kalm_est,&y_kalm_est);
//						vx_kalm_est = _other_uav_pos.vx;
//						vy_kalm_est = _other_uav_pos.vy;
//						got_other_uav_pos = true;
//					}
//
//				}

				hrt_abstime t = hrt_absolute_time();
			    dt = t_prev != 0 ? (t - t_prev) * 0.000001f : 0.005f;
				t_prev = t;

				loop_count++;

//				_att.pitch = _att_sp.pitch_body;
//				_att.roll  = _att_sp.roll_body;
//				_att.yaw  = _att_sp.yaw_body;

				_att.pitchspeed += _act.control[1]*dt ;
				_att.pitch += _att.pitchspeed * dt;

				_att.rollspeed += _act.control[0]*dt;
				_att.roll += _att.rollspeed * dt;


				_att.yaw += _att.yawspeed * dt;
				_att.yawspeed += _act.control[2]*dt;
				_att.yaw = _wrap_pi(_att.yaw);


				if(isfinite(_att_sp.thrust))
				{
					thrust = _att_sp.thrust;
				}
				else
				{
					thrust = 0.0f;
				}



				_local_pos.vz = _local_pos.vz - 9.8f * (thrust - 0.5f/* + 0.3f*sinf(loop_count/2000.0f)*/)*dt; // vz
				if(_local_pos.z >= 0.0f && _local_pos.vz >= 0.0f)
				{
					_local_pos.vz = 0.0f;
					_local_pos.vx = 0.0f;
					_local_pos.vy = 0.0f;
					_att.pitch 	  = 0.0f;
					_att.roll     = 0.0f;
				}
				_local_pos.z = _local_pos.z + _local_pos.vz*dt;

				if(_local_pos.z >= 0.0f)
				{
					_local_pos.z = 0.0f;
				}


//				if(_att_sp.thrust > 0.1f)
//					_local_pos.landed = false;
//				else
//					_local_pos.landed = true;

				alt_avg += (-_local_pos.z - alt_avg) * dt / 3.0f;
				float alt_disp2 = - _local_pos.z - alt_avg;
				alt_disp2 = alt_disp2 * alt_disp2;
				float land_disp2 = 0.7f * 0.7f;

				if (landed) {
						//	if (alt_disp2 > land_disp2 || thrust > params.land_thr) {
							if(_att_sp.thrust  > 0.3f){ //kangli
								landed = false;
								landed_time = 0;
							}

						} else {
							if (alt_disp2 < land_disp2 && thrust < 0.2f) {
								if (landed_time == 0) {
									landed_time = t;    // land detected first time

								} else {
									if (t > landed_time + 3 * 1000000.0f) {
										landed = true;
										landed_time = 0;
									}
								}

							} else {
								landed_time = 0;
							}
						}

				_local_pos.landed = landed;

				accx_body = sinf(-_att.pitch)*9.8f;
				accy_body = sinf(_att.roll)*9.8f;







				_local_pos.x = _local_pos.x + _local_pos.vx * dt + 0.5f*accx_global*dt*dt;
				_local_pos.y = _local_pos.y + _local_pos.vy * dt + 0.5f*accx_global*dt*dt;

				_local_pos.vx = accx_global*dt + _local_pos.vx;
				_local_pos.vy = accy_global*dt + _local_pos.vy;


				accx_global = cosf(_att.yaw)*accx_body - sinf(_att.yaw)*accy_body /* + 3.0f*sinf(loop_count/2000.0f)*/;
				accy_global = sinf(_att.yaw)*accx_body + cosf(_att.yaw)*accy_body /* + 3.0f*sinf(loop_count/2000.0f)*/;


				_local_pos.eph = 1.0f;
				_local_pos.epv = 1.0f;
				_local_pos.ref_alt = 0.0f;
				_local_pos.ref_lat = _home.lat;
				_local_pos.ref_lon = _home.lon;
				_local_pos.v_xy_valid = true;
				_local_pos.v_z_valid = true;
				_local_pos.timestamp = hrt_absolute_time();
				_local_pos.z_valid = true;




				_local_pos.xy_global = true;
				_local_pos.z_global = true;
				_local_pos.yaw = _att.yaw;


				map_projection_reproject(&ref, _local_pos.x, _local_pos.y, &_global_pos.lat, &_global_pos.lon);
				_global_pos.alt = _local_pos.ref_alt - _local_pos.z;
				_global_pos.vel_n = _local_pos.vx;
				_global_pos.vel_e = _local_pos.vy;
				_global_pos.vel_d = _local_pos.vz;

				_global_pos.yaw = _local_pos.yaw;
				_global_pos.timestamp = hrt_absolute_time();

				_global_pos.eph = 1.0f;
				_global_pos.epv = 1.0f;


//				if(got_other_uav_pos){
//
//					float temp_x,temp_y;
//					x_kalm_est += vx_kalm_est * dt;
//					y_kalm_est += vy_kalm_est * dt;
//
//					map_projection_project(&ref,_other_uav_pos.lat,_other_uav_pos.lon,&temp_x,&temp_y);
//
//
//                    x_kalm_est  +=  0.00147523f * (temp_x - x_kalm_est) + 0.0047351f * (_other_uav_pos.vx - vx_kalm_est);  // Kalman filter gain H = [ 0.001475238631117   0.004735125976584
//                    vx_kalm_est +=  0.00047351f * (temp_x - x_kalm_est) + 0.0487420f * (_other_uav_pos.vx - vx_kalm_est);   //					      0.000473512597658   0.048742016195202 ];
//
//                    y_kalm_est  +=  0.00147523f * (temp_y - y_kalm_est) + 0.0047351f * (_other_uav_pos.vy - vy_kalm_est);  // Kalman filter gain H = [ 0.001475238631117   0.004735125976584
//                    vy_kalm_est +=  0.00047351f * (temp_y - y_kalm_est) + 0.0487420f * (_other_uav_pos.vy - vy_kalm_est);   //					      0.000473512597658   0.048742016195202 ];
//
//					_local_pos.x_other_uav =  x_kalm_est;  // G2B
//					_local_pos.y_other_uav =  y_kalm_est;
//					_local_pos.z_other_uav =_local_pos.ref_alt -  _other_uav_pos.alt;
//
//
//					_local_pos.vx_other_uav =   vx_kalm_est;
//					_local_pos.vy_other_uav =   vy_kalm_est;
//					_local_pos.vz_other_uav =    _other_uav_pos.vz;
//
//					_local_pos.yaw_other_uav =  _other_uav_pos.yaw;
//
//
//				}

				_att_temp = _att;
				_att_temp.pitch += 0.01f*rand()/32768 /*+ 0.2f*sinf(loop_count/2000.0f)*/;
				_att_temp.roll += 0.01f*rand()/32768;
				_att_temp.yaw += 0.01f*rand()/32768;

				float cp = cosf(_att_temp.pitch);
				float sp = sinf(_att_temp.pitch);
				float sr = sinf(_att_temp.roll);
				float cr = cosf(_att_temp.roll);
				float sy = sinf(_att_temp.yaw);
				float cy = cosf(_att_temp.yaw);

				_att_temp.R[0][0] = cp * cy;
				_att_temp.R[0][1] = (sr * sp * cy) - (cr * sy);
				_att_temp.R[0][2] = (cr * sp * cy) + (sr * sy);
				_att_temp.R[1][0] = cp * sy;
				_att_temp.R[1][1] = (sr * sp * sy) + (cr * cy);
				_att_temp.R[1][2] = (cr * sp * sy) - (sr * cy);
				_att_temp.R[2][0] = -sp;
				_att_temp.R[2][1] = sr * cp;
				_att_temp.R[2][2] = cr * cp;

				_att_temp.R_valid = true;

				orb_publish(ORB_ID(vehicle_local_position),_local_pos_pub,&_local_pos);
				orb_publish(ORB_ID(vehicle_global_position),_global_pos_pub,&_global_pos);
				orb_publish(ORB_ID(vehicle_attitude),_att_pub,&_att_temp);


	}
	thread_running = false;
	return 0;
}

int px4_simulation_main(int argc, char *argv[])
{
	if (argc < 1)
		usage("missing command");

	if (!strcmp(argv[1], "start")) {

		if (thread_running) {
			warnx("daemon already running\n");
			/* this is not an error */
			return 0;
		}
		thread_should_exit = false;
		daemon_task = task_spawn_cmd("px4_simulation",
					 SCHED_DEFAULT,
					 SCHED_PRIORITY_DEFAULT,
					 2048,
					 px4_simulation_thread_main,
					 (argv) ? (const char **)&argv[2] : (const char **)NULL);
		return 0;
	}

	if (!strcmp(argv[1], "stop")) {
		thread_should_exit = true;
		return 0;
	}

	if (!strcmp(argv[1], "status")) {
		if (thread_running) {
			warnx("\trunning\n");
		} else {
			warnx("\tnot started\n");
		}
		return 0;
	}

	usage("unrecognized command");
	return 1;
}
